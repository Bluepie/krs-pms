﻿namespace krs
{
    partial class OptionAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnwholedatbase = new System.Windows.Forms.Button();
            this.btnapecifieduser = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(61, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Show Whole database";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(39, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(193, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "Search for a specified User";
            // 
            // btnwholedatbase
            // 
            this.btnwholedatbase.Location = new System.Drawing.Point(259, 60);
            this.btnwholedatbase.Name = "btnwholedatbase";
            this.btnwholedatbase.Size = new System.Drawing.Size(75, 23);
            this.btnwholedatbase.TabIndex = 2;
            this.btnwholedatbase.Text = "show";
            this.btnwholedatbase.UseVisualStyleBackColor = true;
            this.btnwholedatbase.Click += new System.EventHandler(this.btnwholedatbase_Click);
            // 
            // btnapecifieduser
            // 
            this.btnapecifieduser.Location = new System.Drawing.Point(259, 118);
            this.btnapecifieduser.Name = "btnapecifieduser";
            this.btnapecifieduser.Size = new System.Drawing.Size(75, 23);
            this.btnapecifieduser.TabIndex = 3;
            this.btnapecifieduser.Text = "show";
            this.btnapecifieduser.UseVisualStyleBackColor = true;
            this.btnapecifieduser.Click += new System.EventHandler(this.btnapecifieduser_Click);
            // 
            // OptionAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 224);
            this.Controls.Add(this.btnapecifieduser);
            this.Controls.Add(this.btnwholedatbase);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "OptionAdmin";
            this.Text = "OptionAdmin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnwholedatbase;
        private System.Windows.Forms.Button btnapecifieduser;
    }
}