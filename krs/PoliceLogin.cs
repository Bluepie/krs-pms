﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace krs
{
    public partial class PoliceLogin : Form
    {
        AdminDatabase connectionObject;
        string cs;
        DataSet dsp;
        string puname;
        string ppassword;
        string DPuname;
        string DPpassword; //DatabasePolice
        public PoliceLogin()
        {
            InitializeComponent();
        }
        //Police PoliceObject = new Police();
        private void btnlogin_Click(object sender, EventArgs e)
        {
            try
            {
                puname = tb_uname.Text;
                ppassword = tb_policepass.Text;
                if (dsp.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsp.Tables[0].Rows.Count; i++)
                    {
                        DPuname = Convert.ToString(dsp.Tables[0].Rows[i]["Username"].ToString());
                        DPpassword = Convert.ToString(dsp.Tables[0].Rows[i]["Password"].ToString());
                        if (puname == DPuname && ppassword == DPpassword)
                        {
                           // PoliceObject.Show();   object destroyed coz of closing
                            new Police().ShowDialog();

                        }
                        else
                        {
                            MessageBox.Show("Username or Passowrd Wrong");
                        }
                    }
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void PoliceLogin_Load(object sender, EventArgs e)
        {
            try
            {
                connectionObject = new AdminDatabase();
                cs = Properties.Settings.Default.PoliceDBCString;
                connectionObject.AdminDBCString = cs;
                connectionObject.sqlAdmin = Properties.Settings.Default.POLICE_SQL;
                dsp = connectionObject.GetAdminConnection;
                Police PoliceObject = new Police();
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void PoliceLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.Close();
           
        }
    }
}
