﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace krs
{
    public partial class UserInfo : Form
    {
        Dbc objConnect;
        string conString;
        DataSet ds;
        DataRow dRow;
        int sp;
        public int specifieduser
        {
            set { sp = value; }
        }
        public UserInfo()
        {
            InitializeComponent();
        }
        
        private void UserInfo_Load(object sender, EventArgs e)
        {
            try
            {
                objConnect = new Dbc();
                conString = Properties.Settings.Default.ECString;
                objConnect.connection_string = conString;
                objConnect.sql = Properties.Settings.Default.SQL;
                ds = objConnect.GetConnection;
                NavigateRecords(sp);

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }
        private void NavigateRecords(int num)
        {
            dRow = ds.Tables[0].Rows[num];

            tbname.Text = dRow.ItemArray.GetValue(0).ToString();
            tbaddress.Text = dRow.ItemArray.GetValue(1).ToString();
            tbgender.Text = dRow.ItemArray.GetValue(2).ToString();
            try
            {
                tbdob.Text = dRow.ItemArray.GetValue(3).ToString();
                tbVerifiedByPolice.Text = dRow.ItemArray.GetValue(5).ToString();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            tbnationality.Text = dRow.ItemArray.GetValue(4).ToString();
            tbVerifiedByPolice.Text = dRow.ItemArray.GetValue(5).ToString();
            tbpassword.Text = dRow.ItemArray.GetValue(6).ToString();
        }

        private void btnlogout_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
