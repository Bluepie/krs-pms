﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace krs
{
    public partial class SearchPerson : Form
    {
        AdminDatabase ConnectionObject;
        string connstring;
        string SearchName;
        public int ParticularName;
        DataSet datset;
        string Puname;

        public SearchPerson()
        {
            InitializeComponent();
        }
        AdminManage ManageObject = new AdminManage();
        private void btnfind_Click(object sender, EventArgs e)
        {
            try
            {
                SearchName = tbSearchName.Text;
                if(datset.Tables[0].Rows.Count > 0)
                {
                    for(int i=0; i < datset.Tables[0].Rows.Count; i++)
                    {
                        Puname = Convert.ToString(datset.Tables[0].Rows[i]["User_name"].ToString());
                        if(SearchName == Puname)
                        {
                            ParticularName = i;
                            ManageObject.ParticularUser = ParticularName; //using already created class by adding property. currently setting property in this area
                            ManageObject.ShowDialog();
                            break;
                        }
                        else
                        {
                            if(i<datset.Tables[0].Rows.Count-1)
                            {
                                continue;
                            }
                            MessageBox.Show("User Not Found. Try Again");
                            break;
                        }
                        
                    }
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void SearchPerson_Load(object sender, EventArgs e)
        {
            try
            {
                ConnectionObject = new AdminDatabase();
                connstring = Properties.Settings.Default.ECString; //for database1
                ConnectionObject.AdminDBCString = connstring;
                ConnectionObject.sqlAdmin = Properties.Settings.Default.SQL;
                datset = ConnectionObject.GetAdminConnection;
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }
    }
}
