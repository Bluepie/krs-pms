﻿namespace krs
{
    partial class UserInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.tbVerifiedByPolice = new System.Windows.Forms.TextBox();
            this.tbgender = new System.Windows.Forms.TextBox();
            this.tbdob = new System.Windows.Forms.TextBox();
            this.tbnationality = new System.Windows.Forms.TextBox();
            this.tbaddress = new System.Windows.Forms.TextBox();
            this.tbname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnlogout = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tbpassword = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Verified By Police";
            // 
            // tbVerifiedByPolice
            // 
            this.tbVerifiedByPolice.Cursor = System.Windows.Forms.Cursors.No;
            this.tbVerifiedByPolice.Enabled = false;
            this.tbVerifiedByPolice.Location = new System.Drawing.Point(188, 207);
            this.tbVerifiedByPolice.Name = "tbVerifiedByPolice";
            this.tbVerifiedByPolice.ReadOnly = true;
            this.tbVerifiedByPolice.Size = new System.Drawing.Size(100, 20);
            this.tbVerifiedByPolice.TabIndex = 20;
            // 
            // tbgender
            // 
            this.tbgender.Enabled = false;
            this.tbgender.Location = new System.Drawing.Point(188, 91);
            this.tbgender.Name = "tbgender";
            this.tbgender.Size = new System.Drawing.Size(100, 20);
            this.tbgender.TabIndex = 30;
            // 
            // tbdob
            // 
            this.tbdob.Enabled = false;
            this.tbdob.Location = new System.Drawing.Point(188, 129);
            this.tbdob.Name = "tbdob";
            this.tbdob.Size = new System.Drawing.Size(100, 20);
            this.tbdob.TabIndex = 29;
            // 
            // tbnationality
            // 
            this.tbnationality.Enabled = false;
            this.tbnationality.Location = new System.Drawing.Point(188, 167);
            this.tbnationality.Name = "tbnationality";
            this.tbnationality.Size = new System.Drawing.Size(100, 20);
            this.tbnationality.TabIndex = 28;
            // 
            // tbaddress
            // 
            this.tbaddress.Enabled = false;
            this.tbaddress.Location = new System.Drawing.Point(362, 85);
            this.tbaddress.Multiline = true;
            this.tbaddress.Name = "tbaddress";
            this.tbaddress.Size = new System.Drawing.Size(144, 120);
            this.tbaddress.TabIndex = 27;
            // 
            // tbname
            // 
            this.tbname.Enabled = false;
            this.tbname.Location = new System.Drawing.Point(188, 20);
            this.tbname.Name = "tbname";
            this.tbname.Size = new System.Drawing.Size(100, 20);
            this.tbname.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(85, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Nationality";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(311, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(92, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Gender";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Date Of  Birth";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(99, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Name";
            // 
            // btnlogout
            // 
            this.btnlogout.Location = new System.Drawing.Point(188, 260);
            this.btnlogout.Name = "btnlogout";
            this.btnlogout.Size = new System.Drawing.Size(75, 23);
            this.btnlogout.TabIndex = 32;
            this.btnlogout.Text = "Logout";
            this.btnlogout.UseVisualStyleBackColor = true;
            this.btnlogout.Click += new System.EventHandler(this.btnlogout_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(88, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "Password";
            // 
            // tbpassword
            // 
            this.tbpassword.Location = new System.Drawing.Point(188, 53);
            this.tbpassword.Name = "tbpassword";
            this.tbpassword.Size = new System.Drawing.Size(100, 20);
            this.tbpassword.TabIndex = 34;
            // 
            // UserInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 305);
            this.Controls.Add(this.tbpassword);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnlogout);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbVerifiedByPolice);
            this.Controls.Add(this.tbgender);
            this.Controls.Add(this.tbdob);
            this.Controls.Add(this.tbnationality);
            this.Controls.Add(this.tbaddress);
            this.Controls.Add(this.tbname);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UserInfo";
            this.Text = "UserInfo";
            this.Load += new System.EventHandler(this.UserInfo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbVerifiedByPolice;
        private System.Windows.Forms.TextBox tbgender;
        private System.Windows.Forms.TextBox tbdob;
        private System.Windows.Forms.TextBox tbnationality;
        private System.Windows.Forms.TextBox tbaddress;
        private System.Windows.Forms.TextBox tbname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnlogout;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbpassword;
    }
}