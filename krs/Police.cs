﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace krs
{
    public partial class Police : Form
    {
        AdminDatabase ConnectionObject;
        DataSet dsu; // storing (fr) from class
        DataRow drow; // for incrementing and decrementing.
        string connectonstring;
        int MaxRows;
        int inc = 0;

        public Police()
        {
            InitializeComponent();
        }
        private void NavigateRrcords()
        {
            drow = dsu.Tables[0].Rows[inc];
            tbname.Text = drow.ItemArray.GetValue(0).ToString();
            tbAddress.Text = drow.ItemArray.GetValue(1).ToString();
            tbPoliceVErification.Text = drow.ItemArray.GetValue(5).ToString();
        }

        private void Police_Load(object sender, EventArgs e)
        {
            try
            {
                ConnectionObject = new AdminDatabase();
                connectonstring = Properties.Settings.Default.ECString;
                ConnectionObject.AdminDBCString = connectonstring;
                ConnectionObject.sqlAdmin = Properties.Settings.Default.SQL;
                dsu = ConnectionObject.GetAdminConnection; //datasetsuer
                MaxRows = dsu.Tables[0].Rows.Count;
                NavigateRrcords();
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
           
        }

        private void btnnext_Click(object sender, EventArgs e)
        {
            if (inc != MaxRows - 1)
            {
                inc++;
                NavigateRrcords();
            }
            else
            {
                MessageBox.Show("End Of Records");
            }
        }

        private void btnprevious_Click(object sender, EventArgs e)
        {
            if (inc > 0)
            {

                inc--;
                NavigateRrcords();

            }
            else
            {

                MessageBox.Show("No previous record");

            }
        }

        private void tbPoliceVErification_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            DataRow row = dsu.Tables[0].Rows[inc];
            row[5] = tbPoliceVErification.Text;
            try
            {
                ConnectionObject.UpdateDatabase(dsu);
                MessageBox.Show("record Updated");
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void Police_FormClosed(object sender, FormClosedEventArgs e)
        {
           // Police PoliceObject = new Police();
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
