﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace krs
{
    public partial class StartLoginPage : Form
    {
        AdminDatabase ConnectionObject;
        string cs;
        DataSet dsu;
        string uname;
        string password;
        string Puname;
        string Ppassword;
        public int SpecificUser;
        public StartLoginPage()
        {
            InitializeComponent();
        }        

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                ConnectionObject = new AdminDatabase();
                cs = Properties.Settings.Default.ECString; //this a connection string for person database
                ConnectionObject.AdminDBCString = cs;
                ConnectionObject.sqlAdmin = Properties.Settings.Default.SQL; //this is select * from Users_table. FOR 'dataset'
                dsu = ConnectionObject.GetAdminConnection;
                
               
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //opens with existing dialogbox
           new Register().ShowDialog();
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        UserInfo UserObject = new UserInfo();
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                uname = tb_uname.Text;
                password = tb_Upassword.Text;
                if (dsu.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsu.Tables[0].Rows.Count; i++)
                    {
                        Puname = Convert.ToString(dsu.Tables[0].Rows[i]["User_name"].ToString());
                        Ppassword = Convert.ToString(dsu.Tables[0].Rows[i]["Password"].ToString());
                        if (uname == Puname && password == Ppassword)
                        {
                            SpecificUser = i;
                            UserObject.specifieduser = SpecificUser; //(e.e) dont judge me
                            UserObject.ShowDialog();
                            break;
                        }
                        else
                        {
                            if (i < dsu.Tables[0].Rows.Count-1)
                            {
                                continue;
                            }
                            
                            MessageBox.Show("Username or Passowrd Wrong");
                            break;
                        }
                    }
                }
            }
            catch (Exception error) 
            {
                MessageBox.Show(error.Message);
            }
                   
        }

        admin loginadmin = new admin(); 
        Police PoliceObject = new Police();
        private void btnloginadmin_Click(object sender, EventArgs e)
        {

            loginadmin.ShowDialog();
           /* if (loginadmin.ShowDialog() == DialogResult.OK)
            {
                MessageBox.Show("");
            }
            * */
        }
        PoliceLogin policelogin = new PoliceLogin();
        private void btnPolice_Click(object sender, EventArgs e)
        {
            policelogin.ShowDialog();
        }

        private void StartLoginPage_Activated(object sender, EventArgs e)
        {
            try
            {
                ConnectionObject = new AdminDatabase();
                cs = Properties.Settings.Default.ECString;
                ConnectionObject.AdminDBCString = cs;
                ConnectionObject.sqlAdmin = Properties.Settings.Default.SQL;
                dsu = ConnectionObject.GetAdminConnection;


            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void StartLoginPage_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                ConnectionObject = new AdminDatabase();
                cs = Properties.Settings.Default.ECString;
                ConnectionObject.AdminDBCString = cs;
                ConnectionObject.sqlAdmin = Properties.Settings.Default.SQL;
                dsu = ConnectionObject.GetAdminConnection;


            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }
    }
}
