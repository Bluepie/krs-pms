﻿namespace krs
{
    partial class StartLoginPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_uname = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_Upassword = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnloginadmin = new System.Windows.Forms.Button();
            this.btnPolice = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb_uname
            // 
            this.tb_uname.AccessibleDescription = "";
            this.tb_uname.Location = new System.Drawing.Point(245, 92);
            this.tb_uname.Name = "tb_uname";
            this.tb_uname.Size = new System.Drawing.Size(217, 20);
            this.tb_uname.TabIndex = 0;
            this.tb_uname.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(172, 202);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Login";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(112, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Password";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "UserID";
            // 
            // tb_Upassword
            // 
            this.tb_Upassword.AccessibleDescription = "";
            this.tb_Upassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tb_Upassword.Location = new System.Drawing.Point(245, 131);
            this.tb_Upassword.Name = "tb_Upassword";
            this.tb_Upassword.PasswordChar = '*';
            this.tb_Upassword.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tb_Upassword.Size = new System.Drawing.Size(217, 20);
            this.tb_Upassword.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(290, 202);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Register";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnloginadmin
            // 
            this.btnloginadmin.Location = new System.Drawing.Point(546, 89);
            this.btnloginadmin.Name = "btnloginadmin";
            this.btnloginadmin.Size = new System.Drawing.Size(75, 23);
            this.btnloginadmin.TabIndex = 6;
            this.btnloginadmin.Text = "admin";
            this.btnloginadmin.UseVisualStyleBackColor = true;
            this.btnloginadmin.Click += new System.EventHandler(this.btnloginadmin_Click);
            // 
            // btnPolice
            // 
            this.btnPolice.Location = new System.Drawing.Point(546, 133);
            this.btnPolice.Name = "btnPolice";
            this.btnPolice.Size = new System.Drawing.Size(75, 23);
            this.btnPolice.TabIndex = 7;
            this.btnPolice.Text = "police";
            this.btnPolice.UseVisualStyleBackColor = true;
            this.btnPolice.Click += new System.EventHandler(this.btnPolice_Click);
            // 
            // StartLoginPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 334);
            this.Controls.Add(this.btnPolice);
            this.Controls.Add(this.btnloginadmin);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tb_Upassword);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tb_uname);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(674, 373);
            this.MinimumSize = new System.Drawing.Size(674, 373);
            this.Name = "StartLoginPage";
            this.Text = "Authentication Page";
            this.Activated += new System.EventHandler(this.StartLoginPage_Activated);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.StartLoginPage_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_uname;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_Upassword;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnloginadmin;
        private System.Windows.Forms.Button btnPolice;
    }
}

