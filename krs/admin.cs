﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace krs
{
    public partial class admin : Form
    {
        AdminDatabase ConnectionObject;
        string ConnectionString; //for the connection string, setup in settings
        DataSet dsa; //datasetAdmin ,put value got from getAdminConnection bcoz its a dataset
        //DataRow dRow; // for rows, col id gud too
        string username;
        string password;
        string Uname; //to store from database
        string Upassword; //to store from database


        public admin()
        {
            InitializeComponent();
        }

        private void admin_Load(object sender, EventArgs e)
        {
            try
            {
                ConnectionObject = new AdminDatabase(); //u know this by "allocating memory"
                ConnectionString = Properties.Settings.Default.AdminDBCString; //from the settings
                ConnectionObject.AdminDBCString = ConnectionString; //property
                ConnectionObject.sqlAdmin = Properties.Settings.Default.ADMIN_SQL;
                dsa = ConnectionObject.GetAdminConnection;//datasetadmin declared as dataset above     
                                                          //+ GetAdminConnection is a property, which returns the rtn value of 'AdminCheck'
                                                          //
            }
            catch(Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            //this.DialogResult = DialogResult.OK;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
           // this.DialogResult = DialogResult.Cancel;
        }

        private void btnalogin_Click(object sender, EventArgs e)
        {
            try
            {
                username = tb_AdminUser.Text;
                password = tb_Admin_password.Text;
                if (dsa.Tables[0].Rows.Count > 0)// ds has property called "Tables" that lists all tables in the ds
                {
                    for (int i = 0; i < dsa.Tables[0].Rows.Count; i++)
                    {
                        Uname = Convert.ToString(dsa.Tables[0].Rows[i]["Username"].ToString()); //from database
                        Upassword = Convert.ToString(dsa.Tables[0].Rows[i]["Password"].ToString()); //same
                        if (username == Uname && password == Upassword)
                        {
                           // new AdminManage().ShowDialog(); //Option added as requested by SIR. reason:showing whole database is 
                                                              //optimal
                            new OptionAdmin().ShowDialog();
                        }                        
                        else
                        {
                            MessageBox.Show("Username or password wrong");
                        }
                    }
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void btnacancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
