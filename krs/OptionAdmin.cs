﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace krs
{
    public partial class OptionAdmin : Form
    {
        public OptionAdmin()
        {
            InitializeComponent();
        }

        private void btnwholedatbase_Click(object sender, EventArgs e)
        {
            new AdminManage().ShowDialog();
        }

        private void btnapecifieduser_Click(object sender, EventArgs e)
        {
            new SearchPerson().ShowDialog();
        }
    }
}
