﻿namespace krs
{
    partial class admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnalogin = new System.Windows.Forms.Button();
            this.btnacancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_Admin_password = new System.Windows.Forms.TextBox();
            this.tb_AdminUser = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnalogin
            // 
            this.btnalogin.Location = new System.Drawing.Point(121, 172);
            this.btnalogin.Name = "btnalogin";
            this.btnalogin.Size = new System.Drawing.Size(75, 23);
            this.btnalogin.TabIndex = 3;
            this.btnalogin.Text = "Login";
            this.btnalogin.UseVisualStyleBackColor = true;
            this.btnalogin.Click += new System.EventHandler(this.btnalogin_Click);
            // 
            // btnacancel
            // 
            this.btnacancel.Location = new System.Drawing.Point(227, 172);
            this.btnacancel.Name = "btnacancel";
            this.btnacancel.Size = new System.Drawing.Size(75, 23);
            this.btnacancel.TabIndex = 4;
            this.btnacancel.Text = "Cancel";
            this.btnacancel.UseVisualStyleBackColor = true;
            this.btnacancel.Click += new System.EventHandler(this.btnacancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(118, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "User";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(108, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Password";
            // 
            // tb_Admin_password
            // 
            this.tb_Admin_password.Location = new System.Drawing.Point(187, 105);
            this.tb_Admin_password.Name = "tb_Admin_password";
            this.tb_Admin_password.PasswordChar = '*';
            this.tb_Admin_password.Size = new System.Drawing.Size(100, 20);
            this.tb_Admin_password.TabIndex = 2;
            // 
            // tb_AdminUser
            // 
            this.tb_AdminUser.Location = new System.Drawing.Point(187, 63);
            this.tb_AdminUser.Name = "tb_AdminUser";
            this.tb_AdminUser.Size = new System.Drawing.Size(100, 20);
            this.tb_AdminUser.TabIndex = 1;
            // 
            // admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 263);
            this.Controls.Add(this.tb_AdminUser);
            this.Controls.Add(this.tb_Admin_password);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnacancel);
            this.Controls.Add(this.btnalogin);
            this.Name = "admin";
            this.Text = "admin";
            this.Load += new System.EventHandler(this.admin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnalogin;
        private System.Windows.Forms.Button btnacancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_Admin_password;
        private System.Windows.Forms.TextBox tb_AdminUser;
    }
}