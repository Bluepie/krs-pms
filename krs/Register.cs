﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace krs
{
    public partial class Register : Form
    {
        Dbc objConnect;
        string conString;
        DataSet ds;
        int inc = 0;
        int MaxRows;
        public Register()
        {
            InitializeComponent();
        }

        private void tbname_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnadd_Click(object sender, EventArgs e)
        {
           
            
        }

       
        private void Register_Load(object sender, EventArgs e)
        {
            try
            {
                objConnect = new Dbc();
                conString = Properties.Settings.Default.ECString;
                objConnect.connection_string = conString;
                objConnect.sql = Properties.Settings.Default.SQL;
                ds = objConnect.GetConnection;
                MaxRows = ds.Tables[0].Rows.Count;
                btnsave1.Enabled = true;
                btncancel1.Enabled = true;


            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void btnsave1_Click(object sender, EventArgs e)
        {
        

            try
            {
                DataRow row = ds.Tables[0].NewRow();
                row[0] = tbname.Text;
                row[1] = tbaddress.Text;
                row[2] = tbgender.Text;
                row[3] = tbdob.Text;
                row[4] = tbnationality.Text;
                row[5] = "Not Verified";
                row[6] = tbpassword.Text;
                ds.Tables[0].Rows.Add(row);
                objConnect.UpdateDatabase(ds);
                MaxRows = MaxRows + 1;
                inc = MaxRows - 1;
                MessageBox.Show("Yor Application Has been submitted");
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

        }

        private void btncancel1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
