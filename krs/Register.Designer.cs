﻿namespace krs
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbgender = new System.Windows.Forms.TextBox();
            this.tbdob = new System.Windows.Forms.TextBox();
            this.tbnationality = new System.Windows.Forms.TextBox();
            this.tbaddress = new System.Windows.Forms.TextBox();
            this.tbname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btncancel1 = new System.Windows.Forms.Button();
            this.btnsave1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tbpassword = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbgender
            // 
            this.tbgender.Location = new System.Drawing.Point(145, 108);
            this.tbgender.Name = "tbgender";
            this.tbgender.Size = new System.Drawing.Size(100, 20);
            this.tbgender.TabIndex = 4;
            // 
            // tbdob
            // 
            this.tbdob.Location = new System.Drawing.Point(145, 143);
            this.tbdob.Name = "tbdob";
            this.tbdob.Size = new System.Drawing.Size(100, 20);
            this.tbdob.TabIndex = 5;
            // 
            // tbnationality
            // 
            this.tbnationality.Location = new System.Drawing.Point(145, 182);
            this.tbnationality.Name = "tbnationality";
            this.tbnationality.Size = new System.Drawing.Size(100, 20);
            this.tbnationality.TabIndex = 6;
            // 
            // tbaddress
            // 
            this.tbaddress.Location = new System.Drawing.Point(319, 40);
            this.tbaddress.Multiline = true;
            this.tbaddress.Name = "tbaddress";
            this.tbaddress.Size = new System.Drawing.Size(222, 147);
            this.tbaddress.TabIndex = 2;
            // 
            // tbname
            // 
            this.tbname.Location = new System.Drawing.Point(145, 31);
            this.tbname.Name = "tbname";
            this.tbname.Size = new System.Drawing.Size(100, 20);
            this.tbname.TabIndex = 1;
            this.tbname.TextChanged += new System.EventHandler(this.tbname_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Nationality";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(268, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Gender";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Date Of  Birth";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Name";
            // 
            // btncancel1
            // 
            this.btncancel1.Enabled = false;
            this.btncancel1.Location = new System.Drawing.Point(224, 229);
            this.btncancel1.Name = "btncancel1";
            this.btncancel1.Size = new System.Drawing.Size(75, 23);
            this.btncancel1.TabIndex = 34;
            this.btncancel1.Text = "Cancel";
            this.btncancel1.UseVisualStyleBackColor = true;
            this.btncancel1.Click += new System.EventHandler(this.btncancel1_Click);
            // 
            // btnsave1
            // 
            this.btnsave1.Enabled = false;
            this.btnsave1.Location = new System.Drawing.Point(81, 229);
            this.btnsave1.Name = "btnsave1";
            this.btnsave1.Size = new System.Drawing.Size(75, 23);
            this.btnsave1.TabIndex = 32;
            this.btnsave1.Text = "Save";
            this.btnsave1.UseVisualStyleBackColor = true;
            this.btnsave1.Click += new System.EventHandler(this.btnsave1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Password";
            // 
            // tbpassword
            // 
            this.tbpassword.Location = new System.Drawing.Point(145, 66);
            this.tbpassword.Name = "tbpassword";
            this.tbpassword.Size = new System.Drawing.Size(100, 20);
            this.tbpassword.TabIndex = 3;
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 286);
            this.Controls.Add(this.tbpassword);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btncancel1);
            this.Controls.Add(this.btnsave1);
            this.Controls.Add(this.tbgender);
            this.Controls.Add(this.tbdob);
            this.Controls.Add(this.tbnationality);
            this.Controls.Add(this.tbaddress);
            this.Controls.Add(this.tbname);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Register";
            this.Text = "Register";
            this.Load += new System.EventHandler(this.Register_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbgender;
        private System.Windows.Forms.TextBox tbdob;
        private System.Windows.Forms.TextBox tbnationality;
        private System.Windows.Forms.TextBox tbaddress;
        private System.Windows.Forms.TextBox tbname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btncancel1;
        private System.Windows.Forms.Button btnsave1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbpassword;
    }
}