﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace krs
{
    public partial class AdminManage : Form
    {
        Dbc objConnect;
        string conString;
        DataSet ds;
        DataRow dRow;
        int MaxRows;
        int inc = 0;
        public AdminManage()
        {
            InitializeComponent();
        }

        public int ParticularUser
        {
            set { inc = value; }
        }
      

        private void Register_Load(object sender, EventArgs e)
        {
            try
            {
                objConnect = new Dbc();
                conString = Properties.Settings.Default.ECString;
                objConnect.connection_string = conString;
                objConnect.sql = Properties.Settings.Default.SQL;
                ds = objConnect.GetConnection;
                MaxRows = ds.Tables[0].Rows.Count;
                NavigateRecords();


            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            
        }
        private void NavigateRecords()
        {
            dRow = ds.Tables[0].Rows[inc];

            tbname.Text = dRow.ItemArray.GetValue(0).ToString();
            tbaddress.Text = dRow.ItemArray.GetValue(1).ToString();
            tbgender.Text = dRow.ItemArray.GetValue(2).ToString();
            try
            {
                tbdob.Text = dRow.ItemArray.GetValue(3).ToString();
                tbVerifiedByPolice.Text = dRow.ItemArray.GetValue(5).ToString();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            tbnationality.Text = dRow.ItemArray.GetValue(4).ToString();
            tbVerifiedByPolice.Text = dRow.ItemArray.GetValue(5).ToString();
            tbpassword.Text = dRow.ItemArray.GetValue(6).ToString();
        }

       

        private void Register_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (inc != MaxRows - 1)
            {
                inc++;
                NavigateRecords();
            }
            else
            {
                MessageBox.Show("End Of Records");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (inc > 0)
            {

                inc--;
                NavigateRecords();

            }
            else
            {

                MessageBox.Show("No previous record");

            }
        }
        private void tbclearfn()
        {
            tbaddress.Clear();
            tbdob.Clear();
            tbgender.Clear();
            tbname.Clear();
            tbnationality.Clear();
            tbVerifiedByPolice.Clear();
            tbpassword.Clear();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            tbclearfn();
            btnadd.Enabled = false;
            btnsave.Enabled = true;
            btncancel.Enabled = true;

        } 

        private void button5_Click(object sender, EventArgs e)
        {
            NavigateRecords();
            btnadd.Enabled = true;
            btncancel.Enabled = false;
            btnsave.Enabled = false;
        }

        private void save_Click(object sender, EventArgs e)
        {
            DataRow row = ds.Tables[0].NewRow();
            row[0] = tbname.Text;
            row[1] = tbaddress.Text;
            row[2] = tbgender.Text;
            row[3] = tbdob.Text;
            row[4] = tbnationality.Text;
            row[5] = "Not Verified";
            row[6] = tbpassword.Text;
            ds.Tables[0].Rows.Add(row);

            try
            {
                objConnect.UpdateDatabase(ds);
                MaxRows = MaxRows + 1;
                inc = MaxRows - 1;
                MessageBox.Show("database upadated");
            }
            catch(Exception err)
            {
                MessageBox.Show(err.Message);
            }
            btncancel.Enabled = false;
            btnsave.Enabled = false;
            btnadd.Enabled = true;
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            DataRow row = ds.Tables[0].Rows[inc];
            row[0] = tbname.Text;
            row[1] = tbaddress.Text;
            row[2] = tbgender.Text;
            row[3] = tbdob.Text;
            row[4] = tbnationality.Text;
            row[6] = tbpassword.Text;
            try
            {
                objConnect.UpdateDatabase(ds);
                MessageBox.Show("record Updated");
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                ds.Tables[0].Rows[inc].Delete();
                objConnect.UpdateDatabase(ds);
                MaxRows = ds.Tables[0].Rows.Count;
                inc--;
                NavigateRecords();
            }
            catch(Exception err)
            {
                MessageBox.Show(err.Message);

            }
        }


        private void AdminManage_Shown(object sender, EventArgs e)
        {
            MessageBox.Show("Welcome");
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
