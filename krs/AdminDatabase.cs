﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace krs
{
    class AdminDatabase
    {
        private string strCon;
        private string sql_string;
        System.Data.SqlClient.SqlDataAdapter da_AdminTable;

        public string AdminDBCString
        {
            set { strCon = value; } //setting value in admin forum,value of the sql connection is set at forum.
        }
        public string sqlAdmin
        {
            set { sql_string = value; } //setting value in admin form, this gets passed to "admincheck()" method.
        }

        public System.Data.DataSet GetAdminConnection//update: (object of this class)object.GetAdminConnection equals admin_data_set.
        {
            get { return AdminCheck(); } // get from admincheck method, if this gets called frm main form using object of this class
                                         //1. goes to method. 2. new connection,copy datatable to dataset variable. 3.passes it here "return"
                                         //4. this returns  THAT to the calling object.
        }
        private System.Data.DataSet AdminCheck()
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(strCon);
            connection.Open();
            da_AdminTable = new System.Data.SqlClient.SqlDataAdapter(sql_string,connection); //2rd oct 2016 practically changed.
            //this is a property... any class can connect to this to any database //for instance   LOGIN PAGE HAS THIS "CONNECTION _OBJECT.SQLADMIN"
            System.Data.DataSet admin_data_set = new System.Data.DataSet();
            da_AdminTable.Fill(admin_data_set, "AdminTableData"); //second parameter is just a name.
            connection.Close();
            return admin_data_set;
        }
        public void UpdateDatabase(System.Data.DataSet ds)
        {
            System.Data.SqlClient.SqlCommandBuilder cb = new System.Data.SqlClient.SqlCommandBuilder(da_AdminTable);
            cb.DataAdapter.Update(ds.Tables[0]);
        }
    }
}
